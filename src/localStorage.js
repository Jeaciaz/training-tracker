export function saveExcerciseSet(set) {
  localStorage.setItem('excerciseSet', JSON.stringify(set));
}

export function getExcerciseSet() {
  return JSON.parse(localStorage.getItem('excerciseSet')) || [];
}

export function saveExcercises(data) {
  const prevData = JSON.parse(localStorage.getItem('excerciseData')) || [];

  prevData.push({ data, date: new Date() });
  localStorage.setItem('excerciseData', JSON.stringify(prevData));
}

export function getExcercises() {
  return JSON.parse(localStorage.getItem('excerciseData')) || [];
}