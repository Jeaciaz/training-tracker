import React from 'react';
import Header from './components/Header';
import NewEntry from './components/NewEntry';
import Logs from './components/Logs';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

function App() {
  return (
    <Router basename="/training-tracker">
      <Header />
      <main style={{padding: '2rem'}}>
        <Switch>
          <Route path="/results">
            <Logs />
          </Route>
          <Route path="/">
            <NewEntry />
          </Route>
        </Switch>
      </main>
    </Router>
  );
}

export default App;
