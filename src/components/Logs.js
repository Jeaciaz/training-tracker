import React from 'react';
import { getExcercises } from '../localStorage';
import './Logs.scss';

export default function Logs() {
  const excercises = getExcercises();

  const excerciseRow = data => data.map((excercise, index) => (
    <tr key={index}>
      <td>{ excercise[0] || '(excercise not specified)' }</td>
      <td>{ excercise[1] || '---' }</td>
    </tr>
  ));

  return excercises.map((excercise, index) => (
    <section key={excercise.date} className="train-entry mb-7">
      <h2 className="train-entry__title">{ new Date(Date.parse(excercise.date)).toDateString() }</h2>
      <table className="train-entry__table">
        <tbody>
          {excerciseRow(excercise.data)}
        </tbody>
      </table>
    </section>
  ));
}
