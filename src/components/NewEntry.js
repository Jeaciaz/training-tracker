import React from 'react';
import {useState} from 'react';
import { saveExcerciseSet, getExcerciseSet, saveExcercises } from '../localStorage';
import trashcan from '../assets/trashcan.svg';
import './NewEntry.scss';

export default function NewEntry() {
  const [excercises, setExcercises] = useState((getExcerciseSet().length ? getExcerciseSet() : ['']).map(excercise => [excercise, '']));

  const setExcercise = (index, name, value) => {
    const otherExcercises = excercises.slice();
    otherExcercises.splice(index, 1, [name, value]);
    setExcercises(otherExcercises);
    saveExcerciseSet(otherExcercises.map(excercise => excercise[0]));
  };

  const addExcercise = () => {
    setExcercises([...excercises, ['', '']]);
  }

  const deleteExcercise = index => {
    const conf = window.confirm('Are you sure you want to delete excercise "' +  excercises[index][0] + '"?');
    if (!conf) return;
    const excercisesClone = excercises.slice();
    excercisesClone.splice(index, 1);
    setExcercises(excercisesClone);
    saveExcerciseSet(excercisesClone.map(excercise => excercise[0]));
  }

  const saveResults = () => {
    saveExcercises(excercises);
  }

  const excerciseFields = excercises.map((excercise, index) => (
    <div key={index} className="form-new__input-row">
      <button className="form-new__btn-delete-field" onClick={() => deleteExcercise(index)}>
        <img src={trashcan} alt="delete"/>
      </button>
      <input 
        className="form-new__input form-new__input_key" 
        type="text" 
        placeholder="Push-ups"
        value={excercise[0]} 
        onChange={event => setExcercise(index, event.target.value, excercise[1])}
      />
      <input 
        className="form-new__input" 
        type="text" 
        placeholder="30s" 
        value={excercise[1]} 
        onChange={event => setExcercise(index, excercise[0], parseInt(event.target.value) || '')}
        onFocus={() => setExcercise(index, excercise[0], parseInt(excercises[index][1]) || '')}
        onBlur={() => setExcercise(index, excercise[0], excercises[index][1] && excercises[index][1] + 's')} 
      />
    </div>
  ))

  return (
    <form onSubmit={event => event.preventDefault()}>
      <div className="form-new">
        {excerciseFields}
        <div className="flex">
          <button className="form-new__btn" onClick={addExcercise}>+</button>
          <button className="form-new__btn ml-auto" onClick={saveResults}>Save results</button>
        </div>
      </div>
    </form>
  )
}
