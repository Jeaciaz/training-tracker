import React from 'react';
import { NavLink } from 'react-router-dom';
import './Header.scss';

export default function Header() {
  return (
    <header className="app-header">
      <h1 className="app-header__title">Training Stuff</h1>
      <div className="app-header__links">
        <NavLink activeStyle={{borderBottom: '2px solid #cbf1f5'}} to="/" exact>New Entry</NavLink>
        <NavLink activeStyle={{borderBottom: '2px solid #cbf1f5'}} to="/results">Log</NavLink>
      </div>
    </header>
  )
}
 